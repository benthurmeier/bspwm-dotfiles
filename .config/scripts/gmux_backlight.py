#!/usr/bin/env python

import re
import subprocess
import optparse

"""
gmux_backlight.py --up 5 -> up 5%
gmux_backlight.py --down 10 -> down 10%
gmux_backlight.py --restore 1 -> restore last backlight value
saves value to plain text file
"""

SYS_BRIGHTNESS_PATH = '/sys/class/backlight/gmux_backlight/brightness'
BL_OUTPUT_PATH = '/home/benjamin/.cache/bl_value.txt'


def main():
    current_brightness = get_current_brightness()
    options = get_arguments()
    if options.restore:
        backlight_value = restore_saved_backlight_value()
        gmux_command(backlight_value)
    elif options.write:
        if current_brightness == 0:
            current_brightness = 153
        write_new_backlight_value(current_brightness)
    else:
        new_backlight_value = get_new_backlight_value(current_brightness, options)
        gmux_command(new_backlight_value)


def get_arguments():
    parser = optparse.OptionParser()
    parser.add_option('--up', dest='up', help='up <value percentage>')
    parser.add_option('--down', dest='down', help='down <value percentage>')
    parser.add_option('--restore', dest='restore', help='restore 1 to restore last brightness level')
    parser.add_option('--write', dest='write', help='write 1 to write current brightness level')
    (options, arguments) = parser.parse_args()
    return options


def get_current_brightness():
    current_brightness_raw = subprocess.check_output(['cat', SYS_BRIGHTNESS_PATH]).decode()
    current_brightness_int = re.search(r'\d*', current_brightness_raw)
    current_brightness = int(current_brightness_int.group(0))
    return current_brightness


def round_nearest_5(x, base=0.05):
    return base * round(x/base)


def get_new_backlight_value(current_value, options):
    current_percentage = current_value / 1023
    adjust_percentage = round_nearest_5(current_percentage)
    if options.up:
        adjust_percentage += int(options.up)/100
    else:
        adjust_percentage -= int(options.down)/100
    if adjust_percentage > 1:
        adjust_percentage = 1.0
    elif adjust_percentage < 0:
        adjust_percentage = 0.0
    adjust_raw = round((1023 * adjust_percentage), 0)
    adjust_raw = str(adjust_raw)[:-2]
    return adjust_raw


def restore_saved_backlight_value():
    with open(BL_OUTPUT_PATH, 'r') as fr:
        for line in fr:
            backlight_value = line.rstrip('\n')
    return backlight_value


def write_new_backlight_value(new_value):
    new_value = str(new_value)
    with open(BL_OUTPUT_PATH, 'w') as fw:
        fw.write(new_value)


def gmux_command(backlight_value):
    subprocess.run('sudo sh -c \"echo ' + backlight_value + ' > ' + SYS_BRIGHTNESS_PATH + '\"', shell=True)


main()
