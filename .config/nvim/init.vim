set background=dark

syntax on
syntax enable

if has("termguicolors")
    set termguicolors
endif
if has("gui_running")
  set guicursor=n-v-c-sm:block,i-ci-ve:block,r-cr-o:blocks
endif
set number
set relativenumber
set hidden
set mouse=a
set noshowmode
set noshowmatch
set nolazyredraw

" Turn off backup
set nobackup
set noswapfile
set nowritebackup

" Search configuration
set ignorecase                    " ignore case when searching
set smartcase                     " turn on smartcase

" Tab and Indent configuration
set expandtab
set tabstop=4
set shiftwidth=4


set clipboard=unnamedplus
let g:clipboard = {
  \   'copy': {
  \      '+': 'xsel -b -i',
  \      '*': 'xsel -p -i',
  \    },
  \   'paste': {
  \      '+': 'xsel -b -o',
  \      '*': 'xsel -p -o',
  \   },
  \   'cache_enabled': 1,
  \ }

nnoremap tn	:tabnew<space>
nnoremap tk	:tabnext<CR>
nnoremap tj	:tabprev<CR>
nnoremap th	:tabfirst<CR>
nnoremap tl	:tablast<CR>

noremap <F3> :Autoformat<CR>

if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')

Plug 'tpope/vim-surround'
Plug 'scrooloose/nerdtree'
Plug 'https://github.com/lambdalisue/suda.vim'
Plug 'Yggdroot/indentLine'
Plug 'w0rp/ale'
Plug 'Chiel92/vim-autoformat'
Plug 'itchyny/lightline.vim'
if has('nvim')
  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
else
  Plug 'Shougo/deoplete.nvim'
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
endif
Plug 'connorholyday/vim-snazzy'
" Plug 'srcery-colors/srcery-vim'
call plug#end()	

" let g:srcery_italic = 1
colorscheme snazzy


let g:deoplete#enable_at_startup = 1

filetype plugin indent on

map <C-o> :NERDTreeToggle ~/Coding<CR>
let NERDTreeQuitOnOpen = 1
let NERDTreeMinimalUI = 1
let NERDTreeDirArrows = 1
let g:NERDTreeIgnore = ['^venv','^__pycache__']

" Ale
let g:ale_lint_on_enter = 0
let g:ale_lint_on_text_changed = 'never'
let g:ale_echo_msg_error_str = 'E'
let g:ale_echo_msg_warning_str = 'W'
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'
let g:ale_linters = {'python': ['flake8']}

let g:lightline = {
      \ 'colorscheme': 'snazzy',
      \ 'active': {
      \   'left': [ ['mode', 'paste'],
      \             ['fugitive', 'readonly', 'filename', 'modified'] ],
      \   'right': [ [ 'lineinfo' ], ['percent'] ]
      \ },
      \ 'component': {
      \   'readonly': '%{&filetype=="help"?"":&readonly?"🔒":""}',
      \   'modified': '%{&filetype=="help"?"":&modified?"+":&modifiable?"":"-"}',
      \   'fugitive': '%{exists("*fugitive#head")?fugitive#head():""}'
      \ },
      \ 'component_visible_condition': {
      \   'readonly': '(&filetype!="help"&& &readonly)',
      \   'modified': '(&filetype!="help"&&(&modified||!&modifiable))',
      \   'fugitive': '(exists("*fugitive#head") && ""!=fugitive#head())'
      \ },
      \ 'separator': { 'left': ' ', 'right': ' ' },
      \ 'subseparator': { 'left': ' ', 'right': ' ' }
      \ }
