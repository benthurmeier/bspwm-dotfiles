autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
# Include hidden files in autocomplete:
_comp_options+=(globdots)


#####################
#POWERLEVEL9K CONFIG#
#####################

ZSH_THEME="powerlevel9k/powerlevel9k"
POWERLEVEL9K_MODE="nerdfont-complete"

POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(root_indicator dir dir_writable virtualenv vcs)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status time)

POWERLEVEL9K_MULTILINE_FIRST_PROMPT_PREFIX=""
POWERLEVEL9K_MULTILINE_LAST_PROMPT_PREFIX=" \uf054 "

#POWERLEVEL9K_PROMPT_ADD_NEWLINE=true
POWERLEVEL9K_PROMPT_ON_NEWLINE=true

POWERLEVEL9K_LEFT_SEGMENT_END_SEPARATOR=""
POWERLEVEL9K_LEFT_SEGMENT_SEPARATOR="\uf0da " 
POWERLEVEL9K_LEFT_SUBSEGMENT_SEPARATOR="\uf0da "
POWERLEVEL9K_RIGHT_SEGMENT_SEPARATOR="\uf0d9"
POWERLEVEL9K_RIGHT_SUBSEGMENT_SEPARATOR="\uf0d9"

POWERLEVEL9K_ROOT_INDICATOR_FOREGROUND="007"
POWERLEVEL9K_ROOT_INDICATOR_BACKGROUND="000"

POWERLEVEL9K_TIME_FORMAT="%D{%H:%M}"
POWERLEVEL9K_ROOT_ICON="\uf303 "
POWERLEVEL9K_SUDO_ICON="\uF09C "
POWERLEVEL9K_HOME_ICON="\uf015 "
POWERLEVEL9K_HOME_FOLDER_ABBREVIATION="\ufc23 "
POWERLEVEL9K_DIR_HOME_FOREGROUND="007"
POWERLEVEL9K_DIR_HOME_BACKGROUND="clear"
POWERLEVEL9K_DIR_HOME_SUBFOLDER_FOREGROUND="007"
POWERLEVEL9K_DIR_HOME_SUBFOLDER_BACKGROUND="clear"
POWERLEVEL9K_DIR_DEFAULT_FOREGROUND="007"
POWERLEVEL9K_DIR_DEFAULT_BACKGROUND="clear"
POWERLEVEL9K_DIR_ETC_FOREGROUND="007"
POWERLEVEL9K_DIR_ETC_BACKGROUND="clear"
POWERLEVEL9K_SHORTEN_DIR_LENGTH=3

POWERLEVEL9K_DIR_WRITABLE_FORBIDDEN_FOREGROUND="001"
POWERLEVEL9K_DIR_WRITABLE_FORBIDDEN_BACKGROUND="clear"

POWERLEVEL9K_VIRTUALENV_ICON="\ue235 "
POWERLEVEL9K_VIRTUALENV_BACKGROUND="clear"
POWERLEVEL9K_VIRTUALENV_FOREGROUND="004"

POWERLEVEL9K_VCS_GIT_GITHUB_ICON="\uf408 "
POWERLEVEL9K_VCS_GIT_GITHUB_ICON="\uf296 "
POWERLEVEL9K_VCS_BRANCH_ICON=""
POWERLEVEL9K_VCS_UNSTAGED_ICON=""
POWERLEVEL9K_VCS_OUTGOING_CHANGES_ICON="\uf55c "
POWERLEVEL9K_VCS_CLEAN_FOREGROUND="007"
POWERLEVEL9K_VCS_CLEAN_BACKGROUND="clear"
POWERLEVEL9K_VCS_UNTRACKED_FOREGROUND="001"
POWERLEVEL9K_VCS_UNTRACKED_BACKGROUND="clear"
POWERLEVEL9K_VCS_MODIFIED_FOREGROUND="013"
POWERLEVEL9K_VCS_MODIFIED_BACKGROUND="clear"

POWERLEVEL9K_STATUS_OK=false
POWERLEVEL9K_STATUS_CROSS=true
POWERLEVEL9K_STATUS_ERROR_FOREGROUND="001"
POWERLEVEL9K_STATUS_ERROR_BACKGROUND="clear"

POWERLEVEL9K_COMMAND_EXECUTION_TIME_BACKGROUND="000"
POWERLEVEL9K_COMMAND_EXECUTION_TIME_FOREGROUND="clear"

POWERLEVEL9K_TIME_FOREGROUND="007"
POWERLEVEL9K_TIME_BACKGROUND="clear"

###########
#OH MY ZSH#
###########
plugins=(cp sudo dircycle dirhistory zsh-autosuggestions zsh-syntax-highlighting)
source $ZSH/oh-my-zsh.sh

ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern cursor)
ZSH_HIGHLIGHT_STYLES[path]='fg=white'

#######
#ALIAS#
#######
alias vim="nvim"
alias grub-update="sudo grub-mkconfig -o /boot/grub/grub.cfg"
alias grub="sudoedit /etc/default/grub"
alias journal="sudo journalctl -b -p 3"
alias neofetch-bong='neofetch --ascii "$(fortune|cowthink -f bong -W 30)"'
alias pacman-backup="sudo pacman -Qe > ~/.config/.backup/pacman_packages"

alias grep="grep --color=auto"
alias pgrep="grep --color=auto"
alias fgrep="fgrep --color=auto"
alias egrep="egrep --color=auto"

alias ls="ls -A --color=auto --group-directories-first"
alias ll="ls -l"

alias kittyconf="vim ~/.config/kitty/kitty.conf"
alias vimrc="vim ~/.config/nvim/init.vim"
alias zshrc="vim ~/.config/zsh/.zshrc"
alias polybarconf="vim ~/.config/polybar/config_bspwm"
alias i3conf="vim ~/.config/i3/config"
alias bspwmconf="vim ~/.config/bspwm/bspwmrc"
alias sxhkdconf="vim ~/.config/sxhkd/sxhkdrc"

alias xresources="vim ~/.Xresources"
alias aliasconf="vim ~/.config/polybar/scripts/alias.py"
alias mirrors="sudoedit /etc/pacman.d/mirrorlist"
alias mirror-update="sudo /usr/bin/reflectorscript"
alias i3exit="i3-msg exit"
alias bspexit="bspc quit"

# run ls on cd 
function chpwd() {
    emulate -L zsh
    ls
}

