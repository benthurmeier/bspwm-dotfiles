#
# wm independent hotkeys
#

# terminal emulator
super + Return
	kitty

# launch neovim
super + Return + shift 
    kitty --class NeoVim nvim

# program launcher
super + @space
    rofi -show drun

super + n
    networkmanager_dmenu

super + c
    rofi -show calc -modi calc -no-show-match -no-sort -no-bold

# make sxhkd reload its configuration files:
super + Escape
	pkill -USR1 -x sxhkd

#
# bspwm hotkeys
#
XF86MonBrightnessUp
    python ~/.config/scripts/gmux_backlight.py --up 5 # increase screen brightness

XF86MonBrightnessDown
    python ~/.config/scripts/gmux_backlight.py --down 5 # increase screen brightness

# Keyboard Brightness
XF86KbdBrightnessUp
    kbdlight up 7%

XF86KbdBrightnessDown
    kbdlight down 7%

# Pulse Audio controls
XF86AudioRaiseVolume
    amixer -D pulse sset Master 5%+ #increase sound volume by 5%

XF86AudioLowerVolume
    amixer -D pulse sset Master 5%- #decrease sound volume by 5%

XF86AudioMute
    amixer -D pulse sset Master toggle # mute sound
# quit/restart bspwm
super + alt + {q,r}
	bspc {quit,wm -r}

#
# bspwm hotkeys
#

# close focussed window
super + w
    bspc node -c

# toogle pseudo tiled
super + shift + t
    bspc node -t tiled

# toggle floating window
super + shift + f
    bspc node -t fullscreen

# cycle windows forward backward
alt + {_,shift + }Tab
    bspc node -f {next,prev}.local

# focus or swap windows
super + {_,shift + }{h,j,k,l}
    bspc node -{f,s} {west,south,north,east}

# Same thing with arrow keys
super + {_,shift + }{Left,Down,Up,Right}
    bspc node -{f,s} {west,south,north,east}

# preselect the splitting area
super + ctrl + {h,j,k,l}
    bspc node -p ~{west,south,north,east}

# same thing with arrow keys
super + ctrl + {Left,Down,Up,Right}
    bspc node -p ~{west,south,north,east}

# move the window into the selected area
super + shift + {a,s,w,d}
    bspc node -n {west,south,north,east}

# expand a window by moving one of its side outward
super + alt + {h,j,k,l}
    bspc node @{west -r -10,south -r +10,north -r -10,east -r +10}

# contract a window by moving one of its side inward
super + alt + shift + {h,j,k,l}
    bspc node @{east -r -10,north -r +10,south -r -10,west -r +10}

# focus last window, desktop
super + {grave,Tab}
    bspc {node,desktop} -f last

# focus desktop / send window to desktop
super + {_,shift + }{1-9,0}
    bspc {desktop -f,node -d} ^{1-9,10}

# next layout (monocle or tiled)
super + t
    bspc desktop -l next

# rotate the leaves of the tree
super + bracket{left,right}
    bspc node @focused:/ -R {270,90}

# flip the desktop tree
super + shift + bracket{left,right}
    bspc node @focused:/ -F {horizontal,vertical}

# select all nodes
super + a
    bspc node -f @/
