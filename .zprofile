export EDITOR="nvim"
export TERMINAL="xterm-kitty"
export BROWSER="brave"
export FILE="ranger"
export ZDOTDIR="$HOME/.config/zsh"
export XDG_CONFIG_HOME="$HOME/.config"
export PYTHONPATH="${PYTHONPATH}:/home/benjamin/.local/bin"
export ZSH="/home/benjamin/.config/oh-my-zsh"
export GTK2_RC_FILES="$HOME/.config/gtk-2.0/gtkrc-2.0"
export LESSHISTFILE="-"

[[ $(fgconsole 2>/dev/null) == 1 ]] && exec startx -- vt1 &> /dev/null

